import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Temperature-Conversion';
  
  farenheit: number = 0;
  celsius: number = 0;

  constructor()
  {
    
  }

  onChangeFarenheit(event: any)
  {
    this.farenheit = event.target.value;
    this.celsius = ( this.farenheit - 32 ) * ( 5 / 9 );
  }

  onChangeCelsius(event: any)
  {
    this.celsius = event.target.value;
    this.farenheit = ( this.celsius * ( 9 / 5 ) + 32 );
  }
}
