# TemperatureConversion

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.25.

## Steps to download the project.
In git terminal/bash, run this command: git clone https://mahesvarier@bitbucket.org/mahesvarier/temperature-conversion.git.
Alternatively, a zipped folder can be downloaded from bitbucket.

## Dependencies
In order to run the project the dependencies are:
1. Npm
2. Node
3. Angular

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Development server
Run `npm install` under Temperature-Conversion folder that consists of package.json file. 

Once `npm install` is successful run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Help
In case the setup cannot be performed or clarification is required please email at `mvpgrand@gmail.com`.